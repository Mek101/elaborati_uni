#include <stdio.h>
#include <math.h>
#include "conversion.h"

#define isBaseSupported(b) ( (b) >= 2 && (b) <= 16 )

/*
	#### PRIVATE FUNCTIONS ####
*/
/*
	Counts the number of digits int the given value.
*/
static void __print(unsigned int value)
{
	if(value >= 10)
	{
		char c;

		switch (value)
		{
			case 10:
				c = 'A';
				break;
			case 11:
				c = 'B';
				break;
			case 12:
				c = 'C';
				break;
			case 13:
				c = 'D';
				break;
			case 14:
				c = 'E';
				break;
			case 15:
				c = 'F';
				break;
			default:
				printf("\t#### ERROR! ####\t");
		}

		printf("%c", c);
	}
	else
		printf("%u", value);
}


static unsigned int __restAt(unsigned int value, unsigned int base, unsigned int i)
{
	unsigned int rest;
	for(; i > 0; i--)
	{
		rest = value % base;
		value = value / base;
	}
	return rest;
}



static unsigned int __getDigitCountInBase(unsigned int value, unsigned int base)
{
	if(value == 0)
		return 1;
	else
		return ceil(log2(value + 1) / log2(base));
}



/*
	#### PUBLIC FUNCTIONS ####
*/
void base_conversion_rc(unsigned int x, unsigned int base)
{
	if(isBaseSupported(base))
	{
		if(x / base != 0)
			base_conversion_rc(x / base, base);

		unsigned int rest = x % base;
		if(base > 10 && rest >= 10)
		{
			char c;

			switch (rest)
			{
				case 10:
					c = 'A';
					break;
				case 11:
					c = 'B';
					break;
				case 12:
					c = 'C';
					break;
				case 13:
					c = 'D';
					break;
				case 14:
					c = 'E';
					break;
				case 15:
					c = 'F';
					break;
				default:
					printf("\t#### ERROR! ####\t");
			}

			printf("%c", c);
		}
		else
			printf("%u", rest);
	}
}



void base_conversion_it(unsigned int x, unsigned int base)
{
	if(isBaseSupported(base))
	{
		unsigned int i;
		for(i = __getDigitCountInBase(x, base); i > 0; i--)
			__print(__restAt(x, base, i));
	}
}
