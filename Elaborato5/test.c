#include <stdio.h>
#include "conversion.h"

void recursive(unsigned int x, unsigned int base)
{
	base_conversion_rc(x, base);
	printf("\n");
}

void iterative(unsigned int x, unsigned int base)
{
	base_conversion_it(x, base);
	printf("\n");
}

int main()
{
	recursive(123456, 2);
	recursive(123456, 12);
	recursive(123456, 16);
	recursive(3, 2);

	printf("\n");

	iterative(123456, 2);
	iterative(123456, 12);
	iterative(123456, 16);
	
	return 0;
}