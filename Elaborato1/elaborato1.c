#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <stdlib.h>


int main()
{
    /* char */
    {
        char a = 'A';    
        printf("TYPE: %-18s NAME: a VALUE: %14c MIN: %+20d MAX: %+20d BYTE: %lu\n", "char", a, CHAR_MIN, CHAR_MAX, sizeof(a));

        signed char b = 'B';
        printf("TYPE: %-18s NAME: b VALUE: %14c MIN: %20d MAX: %+20d BYTE: %lu\n", "signed char", b, SCHAR_MIN, SCHAR_MAX, sizeof(b));
        
        unsigned char c = 'C';
        printf("TYPE: %-18s NAME: c VALUE: %14c MIN: %20d MAX: %+20d BYTE: %lu\n", "unsigned char", c, 0, UCHAR_MAX, sizeof(b));
    }

    /* int */
    {
        signed int d = 15;
        printf("TYPE: %-18s NAME: d VALUE: %14d MIN: %20d MAX: %+20d BYTE: %lu\n", "signed int", d, INT_MIN, INT_MAX, sizeof(d));

        unsigned int e = 15;
        printf("TYPE: %-18s NAME: e VALUE: %14u MIN: %20u MAX: %20u BYTE: %lu\n", "unsigned int", e, 0, UINT_MAX, sizeof(e));

        /*short int*/
        signed short int f = 15;
        printf("TYPE: %-18s NAME: f VALUE: %14d MIN: %20d MAX: %20d BYTE: %lu\n", "signed short int", f, SHRT_MIN, SHRT_MAX, sizeof(f));

        unsigned short int g = 15;
        printf("TYPE: %-18s NAME: g VALUE: %14u MIN: %20u MAX: %20u BYTE: %lu\n", "unsigned short int", g, 0, USHRT_MAX, sizeof(g));

        /*long int*/
        signed long int h = 15;
        printf("TYPE: %-18s NAME: h VALUE: %14li MIN: %20li MAX: %20li BYTE: %lu\n", "signed long int", h, LONG_MIN, LONG_MAX, sizeof(h));

        unsigned long int i = 15;
        printf("TYPE: %-18s NAME: i VALUE: %14lu MIN: %20lu MAX: %20lu BYTE: %lu\n", "unsigned long int", i, 0l, ULONG_MAX, sizeof(i));
    }

    /* float */
    {
        float j = 100.5;
        printf("TYPE: %-18s NAME: j VALUE: %14e MIN: %20e MAX: %20e BYTE: %lu\n", "float", j, FLT_MIN, FLT_MAX, sizeof(j));
    }

    /* double */
    {
        double k = 505.80;
        printf("TYPE: %-18s NAME: k VALUE: %14e MIN: %20e MAX: %20e BYTE: %lu\n", "double", k, DBL_MIN, DBL_MAX, sizeof(k));

        long double l = 505.80L;
        printf("TYPE: %-18s NAME: l VALUE: %14Le MIN: %20Le MAX: %20Le BYTE: %lu\n", "long double", l, LDBL_MIN, LDBL_MAX, sizeof(l));
    }

    return 0;
}