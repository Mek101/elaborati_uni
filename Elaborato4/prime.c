#include <limits.h>
#include <stdio.h>
#include "prime.h"

/*#define DEBUG*/

/* Returns half the number plus it's rest and 1. */
#define largestHalf(n) ( (n / 2) + (n % 2) + 1 )

/*
	#### PRIVATE FUNCTIONS AND MEMBERS ####
*/
/*
	Gets the first prime number after the given offset. In case of overflow, returns 0.
*/
static unsigned short _get_prime_after(const unsigned short offset)
{
	if(offset < 2)
		return 2;
	else
	{
		/*
			Function implementation:
			Makes a candidate prime number major than the offset by testing odd numbers (since the only prime pair number is
			2), then tries to test it incrementing it by 2 at each faliure. If any of the numbers is a prime one, then exits
			both loops and returns it. Id it reaches the maximum value of the data type and it hasn't found a prime number
			in it, then it's impossible to continue since an overflow may occur: so it returns 0, a number ouside the prime
			enseble by definition.
		*/
		char foundPrime = 0; /* 0 if a prime number hasn't been found yet. */

		unsigned short candidate = 0;
		/* If the offset is odd, make the candidate pair to add 1 later on. */
		if(offset % 2 != 0 && offset > 0)
			candidate++;
		
		for(candidate += offset + 1; candidate < USHRT_MAX; candidate += 2)
		{
			foundPrime = is_prime(candidate);	
			#ifdef DEBUG
			printf(" _get_odd_prime_after::candidate: %u, is prime: %d\n", candidate, foundPrime);
			#endif
			if(foundPrime)
				break;
		}

		/* If it terminated because it reached the maximum value without finding a prime, then it overflew: so returning error. */
		if(!foundPrime)
			return 0;
		else
			return candidate;
	}
}


/*
	Establishes if 2 ordinated numbers different than 0 are co-prime between themselfs.
	Returns 1 if they are, 0 otherwise.
*/
static unsigned short _are_co_prime(const unsigned short min, const unsigned short max)
{
	if(min == 0 || max == 0 || max % min == 0)
		return 0;

	/*
		Function implementation:
		Keeping an index for extracting a sequence of prime numbers. Keep extracting prime numbers until either both min
		and max are divided by the same prime or we extract a prime bigger or equal than the largest half of min, which's
		impossible to be to divide min with, and it cannot be min since we already tested min as divisor before the loop;
		and therefore, we end the loop.
	*/
	unsigned short lastPrime = 0; /* The latest extracted prime number. */

	/* Half min plus it's rest, since the all the divisors of a number always less or equal it's half. */
	unsigned short largestHalf = largestHalf(min);
	/* Index for extracting prime numbers. */
	unsigned short i;
	while(lastPrime < largestHalf)
	{
		lastPrime = _get_prime_after(lastPrime);
		#ifdef DEBUG
		printf(" _are_co_prime::index: %u, last test prime number: %u, min: %u, max: %u\n", i, lastPrime, min, max);
		#endif
		/* If both are divided by the same prime number: break the loop and return 0, min and max are not co-prime. */
		if(min % lastPrime == 0 && max % lastPrime == 0)
			return 0;
	}

	return 1;
}



/*
	#### PUBLIC FUNCTIONS AND MEMBERS ####
*/
/* Ritorna 1 se n e' primo, 0 altrimenti. */
unsigned short int is_prime(unsigned short int n)
{
	/* Tests particular cases: 2 is the only pair prime number, while 0 and 1 are excluded by definition and any other pair number isn't prime. */
	if(n == 2)
		return 1;
	else if(n < 2 || n % 2 == 0)
		return 0;
	else
	{
		unsigned short i;
		/* Half min plus it's rest, since the all the divisors of a number arealways less or equal it's half. */
		unsigned short largestHalf = largestHalf(n);
		for(i = 3; i < largestHalf; i += 2)
		{
			/* If it's divisible for any odd number between 3 and itself, then it's not a prime. */
			if(n % i == 0)
				return 0;
		}
		return 1;
	}
}


/*
 * Ritorna l'n-esimo primo, contando a partire da 0.
 *
 * Se il numero e' troppo grande per essere rappresentato
 * con un unsigned short int, ritorna 0.
 */
unsigned short int nth_prime(unsigned short int n)
{
	unsigned short count; /* The number of prime numbers extracted. Opposed to 'n'. */
	unsigned short lastPrime = 0; /* The latest candidate to be prime. 0 if there was an overflow. */

	/*
		Keeps extracting prime number until it has reached the extablished count ('n') or the generator overflew.
	*/
	count = 0;
	while(1)
	{
		lastPrime = _get_prime_after(lastPrime);

		if(count >= n || lastPrime == 0)
			return lastPrime;

		count++;
	}
}


/* Ritorna la successione di numeri primi.
 * La prima chiamata ritorna 2, la seconda 3, ecc.
 * 
 * Se il parametro reset e' diverso da 0, allora la
 * successione viene resettata e la funzione ritorna 2. 
 * Diversamente, la funzione ritorna il primo successivo
 * a quello ritornato alla chiamata precedente.
 *
 * Se il primo successivo e' troppo grande per essere
 * rappresentato con un unsigned short int, la funzione
 * ritorna 0 e la seccessione viene resettata.
 */
unsigned short int succ_prime(int reset)
{
	/* Latest extracted prime number. 0 if there was an overflow. */
	static unsigned short lastPrime = 0;
	if(reset) lastPrime = 0;

	lastPrime = _get_prime_after(lastPrime);
	
	return lastPrime;
}


/* Ritorna 1 se m e n sono coprimi, 0 altrimenti. */
unsigned short int co_prime(unsigned short int m, unsigned short int n)
{
	if(m < n)
		return _are_co_prime(m, n);
	else
		return _are_co_prime(n, m);
}
