#include <stdio.h>
#include "prime.h"

#define printIsPrime(n) ( printf("Is prime %d? Res: %d\n", (n), is_prime((n))) )
#define printAreCoprime(a, b) ( printf("Are coprime: %u, %u? Res: %u\n", (a), (b), co_prime((a), (b))) )

int main()
{
	/* Index for tests. */
	unsigned short i;

	for(i = 0; i < 20; i++)
		printIsPrime(i);
	
	printIsPrime(8191);
	printIsPrime(999);
	printIsPrime(29);
	printIsPrime((unsigned short)65536U);


	for(i = 0; i < 10; i++)
		printf("%u n-ish prime number: %u\n", i, nth_prime(i));

	for(i = 0; i < 10; i++)
		printf("Successive %u prime number: %u\n", i, succ_prime(0));

	printf("succ_prime reset at: %u\n", succ_prime(1));
	
	for(i = 1; i < 10; i++)
		printf("Successive %u prime number: %u\n", i, succ_prime(0));
	
	printAreCoprime(2, 3);
	printAreCoprime(4, 9);
	printAreCoprime(2, 4);
	printAreCoprime(40, 33);
	printAreCoprime(29453, 38333);
	
	return 0;
}
