#include <math.h>

/*
	#### PRIVATE SUPPORT MACROS ####
*/
#define ERR 0./0.


#define _DELTA(a, b, c) (		\
	pow((b), 2) -4 * (a) * (c)	\
)


#define _HAS_SOULUTIONS(a, b, c) (	\
	_DELTA( (a), (b), (c) ) >= 0	\
)


#define _ROOT_OF(a, b, c, op) (									\
	_HAS_SOULUTIONS( (a), (b), (c) ) ?							\
	(															\
		(-(b) op sqrt(_DELTA( (a), (b), (c) ))) / (2 * (a) )	\
	)															\
	 :															\
		ERR														\
)


#define _ROOT_POSITIVE(a, b, c) _ROOT_OF(a, b, c, +)


#define _ROOT_NEGATIVE(a, b, c) _ROOT_OF(a, b, c, -)


#define _SOLVE(a, b, c, op) (									\
	(_ROOT_POSITIVE(a, b, c) op _ROOT_NEGATIVE(a, b, c)) ?		\
		_ROOT_POSITIVE(a, b, c)									\
	 :															\
		_ROOT_NEGATIVE(a, b, c)									\
)



/*
	#### PUBLIC MACROS ####
*/
/* Returns the number of results of the given second-degree polynomial. */
#define NUM_OF_ROOTS(a, b, c) (				\
	_HAS_SOULUTIONS( (a), (b), (c) ) ?		\
	(										\
		_DELTA( (a), (b), (c) ) == 0 ?		\
			1								\
		 :									\
			2								\
	)										\
	 :										\
		0									\
)


/* Returns the result of the given second-degree polynomial for the positive root of it's delta. */
#define ROOT1(a, b, c) (	\
	_SOLVE(a, b, c, >)		\
)


/* Returns the result of the given second-degree polynomial for the negative root of it's delta. */
#define ROOT2(a, b, c) (		\
	_SOLVE(a, b, c, <)			\
)


/* Returns the extreme point of the given second-degree polynomial. */
#define EXTREME_POINT(a, b, c) (	\
	-(b) / (2 * (a))				\
)


/* Of the given second-degree polynomial extreme point, return 1 it it's a maximum point, 0 otherwise. */
#define MAXIMUM_POINT(a, b, c) (	\
	(2 * (a)) < 0					\
)
