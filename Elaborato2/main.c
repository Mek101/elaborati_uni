#include <stdio.h>
#include <math.h>
#include "quadratic_eq.h"

#define isOk(testVal, counter) ( isOk_Impl(__LINE__, testVal, counter) )

void isOk_Impl(const unsigned long line, const double testVal, const double counter)
{
	if(testVal == counter)
		printf("At %lu, is true (%f).\n", line, counter);
	else
		printf("At %lu, is false (%f instead of %f).\n", line, testVal, counter);
}


int main()
{
	isOk(NUM_OF_ROOTS(( (double)1 / 2 ), -2.0, 1.5), 2);
	isOk(NUM_OF_ROOTS(0.5, -2.0, 2.0),				 1);
	isOk(NUM_OF_ROOTS(-0.5, -2.0, -3.0),			 0);

	printf("\n");

	isOk(ROOT1(0.5, -2.0, 1.5),						 3.0);
	isOk(ROOT1(0.5,-2.0,2.0),						 2.0);
	isOk(ROOT1(-0.5,-2.0,-3.0),						 ERR);

	printf("\n");

	isOk(ROOT2(0.5,-2.0,1.5),						 1.0);
	isOk(ROOT2(0.5,-2.0,2.0),						 2.0);
	isOk(ROOT2(-0.5,-2.0,-3.0),						 ERR);

	printf("\n");

	isOk(EXTREME_POINT(0.5,-2.0,1.5),				 2.0);
	isOk(EXTREME_POINT(0.5,-2.0,2),					 2.0);
	isOk(EXTREME_POINT(-0.5,-2.0,-3),				 -2.0);

	printf("\n");

	isOk(MAXIMUM_POINT(0.5,-2.0,1.5),				 0);
	isOk(MAXIMUM_POINT(0.5,-2.0,2),					 0);
	isOk(MAXIMUM_POINT(-0.5,-2.0,-3),				 1);

	printf("\n");

	isOk(ROOT1(0.5, 2.0, -0.5), 					 3.73);
	isOk(ROOT2(-1.0, 1.0, 0.0), 					 1);
}
