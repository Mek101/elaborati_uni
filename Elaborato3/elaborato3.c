/* Main function of the C program. */
#include <math.h>
#include <stdio.h>

int main() {
	unsigned int x;
	unsigned int res1=0, res2=0, res3=0;

	//input
	printf("Enter a positive integer: "); 
	scanf("%u", &x);
	//calculate here

	unsigned int numOfDigits = 0;


	/*
		Calculating res3: number of zero digits in a number.
		Counting when the module of 'y' and 10 is 0, meaning there wasn't any rest in the division, meaning the digit
		was a zero. Then it passes to the next digit by dividing everything by 10.
	*/
	{
		/*
			Calculating res3: number of zero digits in a number.
			Counting when the module of 'y' and 10 is 0, meaning there wasn't any rest in the division, meaning the digit
			was a zero. Then it passes to the next digit by dividing everything by 10.
		*/
		{
			/* Making a copy of x. */
			unsigned int y = x;
			do
			{
				if((y % 10) == 0) /* We have a zero digit. */ 
					res3++;
			}
			while(y /= 10);
		}
		

		/*
			Calculating res2: complement to ten of 'x'
			Advancing digit to digit by dividing everything by 10, until it reaches 0 to obtain the number of digits. Then,
			elevate 10 to the power of the number of digits and subtract the starting number.
		*/
		{
			unsigned int y = x;
			unsigned int numOfDigits = 1;
			while((y /= 10) > 0)
				numOfDigits++;
			res2 = pow(10, numOfDigits) - x;
		}
		while ((y /= 10) > 0);
		res2 = pow(10, numOfDigits) - x;
	}


		/*
			Calculation res3: reverse of a number.
			Making a local copy of 'x'. For each cycle: the result digits are pushed foward by 1 position by multiplying the by 10, then the number's module by 10 is extracted and pushed to the result (since it's a single digits, it's pushed by adding it to the result), and in the end, the number's first digit is removed by dividing it by 10. If the number is still different than 0, repeat.
		*/
		{
			unsigned int y;
			for(y = x; y != 0 ; y /= 10)
			{
				/* Advancing the result digits. */
				res1 *= 10;
				/* Getting then pushing the digit from the source to the result. */
				res1 += y % 10;
			}
		}
	}




	//output
	printf("%u %u %u\n", res1, res2, res3);

	//no code after this point
	#ifdef EVAL
		eval(x,res1, res2, res3);
	#endif
	return 0;
}
